import { Component } from '@angular/core';
import { NavController, ToastController, LoadingController } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';

declare let plugins;

@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})
export class HomePage {

	private _hasShownInfoToast: boolean;
	private _currentToast: any;
    public isScanning = false;
	public locations = [];
	public beacons = [];

	private _options = {
		disabledCategories: [],
		tags: {},
		allowBackgroundScanning: true,			// Android only
		notifications: {
			showNotifications: true,
			textColor: "#ff9900", 				// Android only
			smallIconName: "notification_icon"	// Android only
		}
	};

	constructor(public navCtrl: NavController,
				public toastCtrl: ToastController,
				public loadingCtrl: LoadingController,
				public browser: InAppBrowser) {

	}

	public startScanning(): void {

		if (this.isScanning) {
			return;
		}

		let loading = this.loadingCtrl.create({
			content: 'Starting SDK'
		});
		loading.present();

		plugins.NexenSDK.startScanning(this._options, (data) => {
			console.log('startScanning', data);
			this.onLocationsLoaded();
			this.onProximityZoneNotification();
			this.onOpenUrl();
			this.isScanning = true;
			loading.dismiss();
		}, (err) => {
			console.log('startScanning failed', err);
			this.isScanning = false;
			loading.dismiss();
		});
	}

    public stopScanning(): void {
        plugins.NexenSDK.stopScanning((data) => {
            console.log('stopScanning', data);
            this.isScanning = false;
        }, (err) => {
            console.log('stopScanning failed', err);
        });
    }

	public onLocationsLoaded(): void {
		plugins.NexenSDK.onLocationsLoaded((data) => {
			console.log('onLocationsLoaded', data);

			// Show a toast as feedback
			if (data && data.length === 1) {
				this._presentToast('Received a location: ' + data[0].name);
			} else if (data && data.length > 1) {
				this._presentToast('Received multiple locations');
			}

			this.locations = data;
		}, (err) => {
			console.log('onLocationsLoaded failed', err);
		});
	}

	public onProximityZoneNotification(): void {
		plugins.NexenSDK.onProximityZoneNotification((data) => {
			this._presentToast('Received a proximity zone notification');
			this.beacons.push(data);

			if (!this._hasShownInfoToast) {
				this._presentToast('Tap a beacon in the list to trigger onPushNotificationTappedForZone');
				this._hasShownInfoToast = true;
			}
			console.log('onProximityZoneNotification', data);
		}, (err) => {
			console.log('onProximityZoneNotification failed', err);
		});
	}

	public onOpenUrl(): void {
		plugins.NexenSDK.onOpenUrl((url) => {
			console.log('onOpenUrl', url);
			this.browser.create(url);
		}, (err) => {
			console.log('onOpenUrl failed', err);
		});
	}

	public onBeaconTapped(beacon: any): void {
		plugins.NexenSDK.onPushNotificationTappedForZone(beacon.id, beacon.contentType, (data) => {
			this._presentToast('Triggered onPushNotificationTappedForZone');
		}, (err) => {
			this._presentToast('Did not find the beacon to trigger the onPushNotificationTappedForZone function');
		});
	}

	private _presentToast(message: string) {

		if (this._currentToast) {
			this._currentToast.dismiss();
		}

		this._currentToast = this.toastCtrl.create({
			duration: 3000,
			position: 'bottom',
			message: message,
			showCloseButton: true
		});

		this._currentToast.present();
	}

}
