#!/usr/bin/env node
'use strict';

// Copyright 2015 Ayogo Health Inc.

// Put this file in ./hooks/before_build/
// Make sure it is executable! (chmod +x)

var filestocopy = [{
    "../resources/android/notification-icon/drawable-hdpi-notification_icon.png":
    "../platforms/android/res/drawable-hdpi/notification_icon.png"
}, {
    "../resources/android/notification-icon/drawable-mdpi-notification_icon.png":
    "../platforms/android/res/drawable-mdpi/notification_icon.png"
}, {
    "../resources/android/notification-icon/drawable-xhdpi-notification_icon.png":
    "../platforms/android/res/drawable-xhdpi/notification_icon.png"
}, {
    "../resources/android/notification-icon/drawable-xxhdpi-notification_icon.png":
    "../platforms/android/res/drawable-xxhdpi/notification_icon.png"
}, {
    "../resources/android/notification-icon/drawable-xxxhdpi-notification_icon.png":
    "../platforms/android/res/drawable-xxxhdpi/notification_icon.png"
}];

var fs = require('fs');
var path = require('path');

// no need to configure below
var rootdir = process.argv[2];

filestocopy.forEach(function(obj) {
    Object.keys(obj).forEach(function(key) {
        var val = obj[key];
        var srcfile = path.join(rootdir, key);
        var destfile = path.join(rootdir, val);
        var destdir = path.dirname(destfile);

        if (!fs.existsSync(destdir)) {
            fs.mkdirSync(destdir);
        }

        if (fs.existsSync(srcfile) && fs.existsSync(destdir)) {
            fs.createReadStream(srcfile).pipe(
               fs.createWriteStream(destfile));
        }
    });
});
