#!/usr/bin/env bash

clear

set -x

# perform a full build
if [[ $* == *--full* ]]; then
    rm -r platforms/
    rm -r plugins/
    rm -r node_modules/
    rm -r www/
fi

# Install dependencies
npm install

# Create a symlink to fix a bug in npm
cd node_modules
ln -s ../local-plugins
cd ..

# Refresh platforms
if [[ $* == *--full* ]]; then
    ionic cordova platform remove ios
    ionic cordova platform remove android
    ionic cordova platform add ios
    ionic cordova platform add android
fi

# Load in the refresh script
. refresh.sh

# Perform the android build
ionic cordova build android --release

# Prepare iOS so the platform folder gets created
ionic cordova prepare ios

# Manually run pod install (work-around)
cd platforms/ios/
pod install
cd ../..

# Perform the iOS build
ionic cordova build ios --release