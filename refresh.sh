#!/usr/bin/env bash

ionic cordova plugin remove cordova-plugin-nexen-sdk
ionic cordova plugin add ./local-plugins/wearenexen-cordova-beacons-plugin/  --variable APP_PASSWORD="3nd4re.adm1n.2017" --variable APP_NAME="EndareAdmin" --variable LOCATION_USAGE_DESCRIPTION="This app uses your location to track beacons." --variable BLUETOOTH_USAGE_DESCRIPTION="Please allow to use Bluetooth to start beacon monitoring"
